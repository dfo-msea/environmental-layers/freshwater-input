import argparse
import freshwater_utils
import sys


def main(inargs):
    """
    Run the program.
    :param inargs: command line arguments from argparse.
    :return:
    """
    freshwater_utils.process(inargs.geo_region,
                             inargs.fwa_container,
                             inargs.filter_file,
                             inargs.out_gpkg,
                             inargs.method,
                             inargs.p,
                             inargs.r,
                             inargs.wmin,
                             inargs.wmax)


if __name__ == '__main__':
    description = 'Generate freshwater input raster for use in species distribution modelling.'
    parser = argparse.ArgumentParser(description=description)

    # Add arguments.
    parser.add_argument('geo_region', type=str,
                        choices=['hg', 'qcs', 'wcvi', 'salish_sea', 'ncc'],
                        help='Name of geographic region or area used for file naming and data preparation.')
    parser.add_argument('fwa_container', type=str,
                        help='FWA container file (gpkg, fgdb).')
    parser.add_argument('filter_file', type=str, 
                        help='Spatial file (shp, tif) or name of gpkg or fgdb file to use as spatial filter against FWA streams. \
                        If gpkg or fgdb, use --filter_lyr to specify layer name.')
    parser.add_argument('out_gpkg', type=str, help='Name of Geopackage to write to.')
    parser.add_argument('method', type=str, choices=['interpolate', 'inverse_proximity', 'convolve'], 
                        help='Method to use for creating raster layer(s) from points. interpolate uses GDAL inverse distance to \
                        a power. inverse_proximity creates proximity rasters and then inverse distance raster algebra. See README \
                        for details.')
    parser.add_argument('-p', type=float, default=None, 
                        help='Power value (float) used in the interpolation or inverse proximity function.')
    parser.add_argument('-r', type=int, default=None,
                        help='Radius used to constrain input points during the interpolation.')
    parser.add_argument('-wmin', type=int, default=None,
                        help='An odd, positive integer value used to define the minimum window size for convolution operation. The value is used to define the shape of the window for the numpy array \
                              which is used to contain the weight values.')
    parser.add_argument('-wmax', type=int, default=None,
                        help='An odd, positive integer value used to define the maximum window size for convolution operation. The value is used to define the shape of the window for the numpy array \
                              which is used to contain the weight values.')                        
    args = parser.parse_args()

    if args.method == 'interpolate' and args.r is None:
        sys.exit('When using the interpolate method, a radius value must be supplied using -r <integer value>.')
    
    if args.method in (['interpolate', 'inverse_proximity']) and args.p is None:
        sys.exit('When using the interpolate or inverse_proximity method, a power value must be supplied using -p <float value>.')
    
    if args.method == 'convolve' and (args.wmin or args.wmax) is None:
        sys.exit('When using the colvolve method, a min and max window size value must be supplied using -wmin <integer value> and  -wmax <integer value>.')
    
    if args.wmin is not None:
        assert (args.wmin > 0 and args.wmin%2 == 1), '-wmin Must be a positive, odd integer.'
    
    if args.wmax is not None:
        assert (args.wmax > 0 and args.wmax%2 == 1), '-wmax Must be a positive, odd integer.'
    
    main(args)
