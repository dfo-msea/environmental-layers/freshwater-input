import os
from datetime import datetime
import logging
import warnings
warnings.simplefilter(action="ignore", category=FutureWarning)
import geopandas as gpd
import rasterio as rio
from shapely.ops import transform
from shapely.prepared import prep
from shapely.geometry import Point, MultiPolygon, Polygon, box
from scipy import ndimage as ndi
from osgeo import gdal, ogr, osr
import numpy as np
import pandas as pd
from math import sqrt
from beautifultable import BeautifulTable
import psutil



# Global variables for logging.
DATE = datetime.now().strftime("%Y-%m-%d")
DATE_TIME = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
LOG_NAME = "freshwater-input"
LOG_STRING = f"LOGGING {LOG_NAME} - {DATE_TIME}"
MSG_BREAK = "-"*50
SUBAREA_BREAK = "#"*50


# Variables for processing.
vector_formats = ["gdb", "shp", "gpkg"]
raster_formats = ["tif", "tiff", "geotiff"]
os.chdir("../")
data_dir = os.path.join(os.getcwd(), "data")

fwa_lyr_names = {"coast": "fwa_coastline",
                 "streams": "fwa_stream_network",
                 "rivers": "fwa_rivers_poly_dissolve_update"}


class ConditionalFormatter(logging.Formatter):
    """Return the log message. If the extra dictionary passed to logger.info has value True for the key "simple", suppress extra formatting (e.g., asctime, levelname).
    logger.info("Starting script...", extra={"simple": True})
    https://stackoverflow.com/questions/34954373/disable-format-for-some-messages
    """

    def format(self, record):
        if hasattr(record, "simple") and record.simple:
            return record.getMessage()
        else:
            return logging.Formatter.format(self, record)


def setup_logger(directory, name=LOG_NAME, date=DATE_TIME, level=logging.INFO):
    """Gets or creates a logger.

    Args:
        directory (str): Directory path for output file
        name (str, optional): Name for logfile. Defaults to LOG_NAME.
        date (datetime, optional): Date to append to logfile name. Defaults to DATE_TIME.
        level (str, optional): Logging level for file. Defaults to logging.INFO.

    Returns:
        logger_obj: Logger object.
    """
    # Gets or creates a logger
    logger_obj = logging.getLogger(name)
    # set log level
    logger_obj.setLevel(level)
    # Create log subdirectory or set the path if it exists.
    logdir = make_dir(directory, "logs")
    # define file handler and set formatter
    file_handler = logging.FileHandler(os.path.join(logdir, f"{name}-{date}.log"))
    formatter = ConditionalFormatter("%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s")
    file_handler.setFormatter(formatter)
    # add file handler to logger
    logger_obj.addHandler(file_handler)
    return logger_obj


def make_dir(working_dir, outname):
    """Create a directory if it does not exist.

    Args:
        working_dir: Current working directory.
        outname: Name for dir to create in base dir.

    Returns: path to new directory.
    """
    outpath = os.path.join(working_dir, outname)
    # Make output directory if it does not exist already
    if not os.path.exists(outpath):
        print(f"Creating directory: {outpath}")
        os.mkdir(outpath)
    else:
        print(f"{outpath} already exists.")
    return outpath


logger = setup_logger(".")

def spatial_type(file_name):
    """Return the spatial type based on extension from FILE_NAME.

    Args:
        file_name (string): Name of spatial file.

    Returns:
        string: "vector" or "raster" depending on file extension.
    """
    assert (file_name.split(".")[-1].lower() in vector_formats + raster_formats), f"File extension for {file_name} should be in {str(vector_formats + raster_formats)}."
    return "vector" if file_name.split(".")[-1] in vector_formats else "raster"


def is_dir(directory):
    """Return DIRECTORY string if it is a directory."""
    assert (os.path.isdir(directory) is True), f"{directory} does not exist."
    return directory


def is_file(file):
    """Return FILE string if it is a file."""
    assert (os.path.isfile(file) is True), f"{file} does not exist."
    return file


def get_driver(file_name):
    """Return driver for reading/writing spatial files.

    Args:
        file_name: Name of file with extension.

    Returns: String for driver.
    """
    assert (file_name.split(".")[-1] in ["shp", "gdb", "gpkg"]), f"File extension for {file_name} should be gdb, shp, or gpkg."
    ext = file_name.split(".")[-1]
    driver_dict = {"shp": "ESRI Shapefile",
                   "gdb": "FileGDB",
                   "gpkg": "GPKG"}
    return driver_dict.get(ext)


def load_raster(data_dir, file_name):
    """Load GeoTIFF file as a rasterio DatasetReader object.

    Args:
        data_dir: directory that contains the raster file.
        file_name: name of the raster file, including file extension.

    Returns: rasterio dataset reader object..
    """
    assert (file_name.split(".")[-1].lower() in raster_formats), f"Only GeoTIFF format supported for raster layers."
    file_path = is_file(os.path.join(data_dir, file_name))
    logger.info(MSG_BREAK)
    logger.info(f"Loading {file_name} as rasterio dataset reader object.")
    return rio.open(file_path)


def get_epsg(source_data):
    """Return EPSG code from raster spatial file.

    Args:
        source_data: spatial file.
    """
    assert (source_data.crs.to_epsg() is not None), f"Source data must have an EPSG code."
    return source_data.crs.to_epsg()


def get_bbox(source_data):
    """Return bounding box geometry for SOURCE_DATA.

    Args:
        source_data: spatial layer.

    Returns: GeoPandas GeoDataFrame object with bounding box geometry.
    """
    spatial_type = "raster" if hasattr(source_data, "name") else "vector"
    logger.info(MSG_BREAK)
    logger.info(f"Creating bounding box geometry.")
    if spatial_type == "vector":
        envelope_gdf = source_data.envelope
        union_geom = envelope_gdf.unary_union
        return gpd.GeoDataFrame({"id": 1, "geometry": [union_geom]}, crs=envelope_gdf.crs)
    else:
        raster_bounds = source_data.bounds
        geom = box(*raster_bounds)
        epsg_code = get_epsg(source_data)
        return gpd.GeoDataFrame({"id": 1, "geometry": [geom]}, crs=f"EPSG:{epsg_code}")


def load_vector(data_dir, file_name, layer_name=None, bbox=None):
    """Return spatial data as geopandas dataframe.

    Args:
        data_dir: data directory.

    Returns: GeoPandas GeoDataFrame object.
    """
    assert (file_name.split(".")[-1] in vector_formats), f"File extension for {file_name} should be gdb, shp, or gpkg."
    assert (layer_name is not None if file_name.split(".")[-1] in ["gdb", "gpkg"] else True), f"No layer_name provided for {file_name}."
    ext = file_name.split(".")[-1]
    def file_or_dir(file_ext): return is_file if file_ext in ["shp", "gpkg"] else is_dir
    file_or_dir_fn = file_or_dir(ext)
    data_dir, spatial_file = is_dir(data_dir), file_or_dir_fn(os.path.join(data_dir, file_name))
    logger.info(MSG_BREAK)
    logger.info(f"Loading {spatial_file} into GeoDataFrame.")
    driver = get_driver(file_name)
    gdf = gpd.read_file(spatial_file, driver=driver, layer=layer_name, bbox=bbox)
    logger.info(f"Number of records in GeoDataFrame: {len(gdf)}")
    return gdf


def write_gpkg(gdf_input, output_dir, gpkg_name, lyr_name):
    """Return path to GeoPackage after writing spatial data to disk.

    Args:
        gdf_input: input geodataframe.
        output_dir: directory to write geopackage to.
        gpkg_name: name for geopackage file.
        lyr_name: output layer name.

    Returns:
        file_path: Filepath to output geopackage.
    """
    assert (gpkg_name.split(".")[-1] == "gpkg"), f"{gpkg_name} does not have GeoPackage extension (gpkg)"
    output_dir = is_dir(output_dir)
    logger.info(MSG_BREAK)
    logger.info(f"Writing GeoDataframe to {gpkg_name} as layer {lyr_name}.")
    file_path = os.path.join(output_dir, gpkg_name)
    gdf_input.to_file(file_path, layer=lyr_name, driver="GPKG")
    return file_path


def cell_resolution(data_directory, raster_path):
    """Return X cell resultion; if X and Y have different resolutions, warn user in logs.

    Args:
        data_directory (str) Path to directory where input data is.
        raster_path (str): Input raster filename.

    Returns:
        int: X cell resolution. 
    """
    raster_ds = gdal.Open(os.path.join(data_directory, raster_path))
    gt = raster_ds.GetGeoTransform()
    x_resolution, y_resolution = gt[1], -gt[5]
    if x_resolution != y_resolution:
        logger.warning(f"Cell X resolution ({y_resolution}) and Y resolution ({y_resolution}) differ. X resolution is used.")
    raster_ds = None
    return int(x_resolution)

def subset_gdf(gdf_input, column, value):
    """Return a GeoDataFrame with results from GDF_INPUT filtered by VALUE on COLUMN.

    Args:
        gdf_input: Geopandas GeoDataFrame object.
        column: column in GeoDataFrame to filter rows on.
        value: filter value used to select rows from column in GeoDataFrame.

    Returns:
        GeoDataFrame: filtered GeoDataFrame.
    """
    logger.info(MSG_BREAK)
    logger.info(f"Filtering input GeoDataframe on column {column} where value == {value}.")
    return gdf_input[gdf_input[column] == value]


def _to_2d(x, y, z):
    """Return tuple with X, Y geometry (remove 3D Z geometry).
    Flatten geometry https://github.com/shapely/shapely/issues/709
    """
    return tuple(filter(None, [x, y]))


def is_3d(gdf_input):
    """Return whether GDF_INPUT has 3D geometry by checking the geom of the first row."""
    return gdf_input.iloc[0].geometry.has_z


def gdf_2d(gdf_input):
    """Return GeoDataframe after replacing 3D geometry in GDF_INPUT with 2D geometry.

    Args:
        gdf_input (GeoDataFrame): Input GeoDataFrame with 3D geometry.

    Returns:
        GeoDataFrame: GeoDataframe with 2D geometry.
    """
    geoms_2d = [transform(_to_2d, row.geometry) for row in gdf_input.itertuples()]
    # Create new dataframe with 2D geometry.
    gdf_input = gdf_input.set_geometry(geoms_2d)
    return gdf_input


def intersect_lines(gdf_input_1, gdf_input_2):
    """Return a GeoDataFrame with point geometry at the intersection(s) of lines from GDF_INPUT_1 and GDF_INPUT_2. Check for duplicates by their geometry and remove."""
    logger.info(MSG_BREAK)
    logger.info(f"Overlaying polyline GeoDataFrames to create points at the intersections. Removing duplicate points by their geometry.")
    gdf_output = gpd.overlay(gdf_input_1, gdf_input_2, how="intersection", keep_geom_type=False)
    gdf_output.drop_duplicates("geometry", inplace=True)
    return gdf_output


def vector_to_raster(raster_path, vector_path, vector_lyr_name, output_dir, nodata_value, region_name, variable, epsg_code):
    """Rasterize a vector layer, given a raster as a template for raster size and projection.

    Args:
        raster_path (str): Path to raster file.
        vector_path (str): Path to container for vector data.
        vector_lyr_name (str): Vector layer name.
        output_dir (str): Output directory.
        nodata_value (int): NoData value for raster (integer since UInt32 data type is specified as output).
        region_name (str): Geographic region name.
        variable (str): Column with values to assign to raster cells: STREAM_ORDER or STREAM_MAGNITUDE.
        epsg_code (int): EPSG projection code.

    Returns:
        str: Filepath to output raster.
    """
    logger.info(MSG_BREAK)
    logger.info("PROCESSING VECTOR TO RASTER...")

    raster_ds = gdal.Open(raster_path)
    vector_ds = ogr.Open(vector_path)
    vector_lyr = vector_ds.GetLayer(vector_lyr_name)
    geotransform = raster_ds.GetGeoTransform()

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg_code)

    output_filename = os.path.join(output_dir, f"{region_name}_{variable.lower()}.tif")
    driver_tiff = gdal.GetDriverByName("GTiff") 
    target_ds = driver_tiff.Create(output_filename, raster_ds.RasterXSize, raster_ds.RasterYSize, 1, gdal.GDT_UInt32)
    target_ds.SetGeoTransform(geotransform)
    target_ds.SetProjection(srs.ExportToWkt())

    # Rasterize vector layer (osgeo.ogr.Layer)
    logger.info("Converting to raster")
    gdal.RasterizeLayer(target_ds, [1], vector_lyr, options=[f"ATTRIBUTE={variable}"])
    target_ds.GetRasterBand(1).SetNoDataValue(nodata_value) 
    target_ds = None

    logger.info("PROCESSING VECTOR TO RASTER... COMPLETE")
    return output_filename


def calc_stats(raster_file):
    """Non-pure function with side effect of computing statistics on single-band raster, returns None.
    
    Args:
        raster_file: Path to source raster file.

    Returns: None
    """
    logger.info(f"Calculating statistics for {raster_file}")
    gdal_dataset = gdal.Open(raster_file)
    gdal_band = gdal_dataset.GetRasterBand(1)
    gdal_band.ComputeStatistics(0)
    gdal_band = None
    gdal_dataset = None
    return None


def proximity_raster(raster_path, cell_value, output_dir, region_name):
    """Create a proximity raster where the cell values represent the distance to a cell with a specified cell value in source raster.

    Args:
        raster_path: Path to source raster file.
        cell_value: Value in raster cell to compute proximity on (i.e., how far is each cell to a cell with value <cell_value>?).
        output_dir: Output directory.
        region_name: Region name.

    Returns: path to proximity raster

    """
    # https://gis.stackexchange.com/questions/300286/geodesic-distance-from-raster-in-python
    # Create a distance to land raster
    logger.info(MSG_BREAK)
    logger.info(f"Creating proximity raster for {region_name.upper()}...")
    src_ds = gdal.Open(raster_path)
    srcband = src_ds.GetRasterBand(1)
    dst_filename = os.path.join(output_dir, f"{region_name}_proximity.tif")

    drv = gdal.GetDriverByName("GTiff")
    dst_ds = drv.Create(dst_filename,
                        src_ds.RasterXSize, src_ds.RasterYSize, 1,
                        gdal.GetDataTypeByName("Float32"))

    dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
    dst_ds.SetProjection(src_ds.GetProjectionRef())
    dstband = dst_ds.GetRasterBand(1)
    # Calculate proximity of each cell to land cell (value==1)
    gdal.ComputeProximity(srcband, dstband, [f"VALUES={cell_value}", "NODATA=-9999.0", "DISTUNITS=PIXEL"])
    srcband = None
    dstband = None
    src_ds = None
    dst_ds = None
    calc_stats(dst_filename)
    return dst_filename


def stream_ord_mag(gdf_input, column):
    """Return a list of unique values from COLUMN in GDF_INPUT.

    Args:
        gdf_input (GeoDataFrame): Input GeoDataFrame.
        column (string): string with value "STREAM_MAGNITUDE" or "STREAM_ORDER".

    Returns:
        (list): list of unique values from column in input GeoDataFrame.
    """
    assert column in ["STREAM_ORDER", "STREAM_MAGNITUDE"], f"Invalid entry {column}. Must be either STREAM_MAGNITUDE or STREAM_ORDER."
    return list(gdf_input[column].unique())


def array2raster(output_dir, numpy_array, source_dataset_path, name_suffix, epsg_code, nodata_value, datatype=None):
    """Return filepath to output raster, created from NumPy array.

    Args:
        output_dir (str): Directory to write GeoTiff file into.
        numpy_array (array): NumPy array to write into GeoTiff raster.
        source_dataset_path (str): Path to GeoTiff, that contains the metadata.
        variable_value (int): The value representing the variable from the FWA, e.g. 1-7 Stream Order values.
        name_suffix (str): String to append to end of file name, e.g. "weighted_inverse".
        epsg_code (int): EPSG code.
        nodata_value: value for NoData.

    Returns:
        str: Filepath to output raster, created from NumPy array.
    """
    source_dataset_reader = rio.open(source_dataset_path)
    dst_filename = os.path.join(output_dir, f"{name_suffix}.tif")
    dtype = datatype if datatype is not None else source_dataset_reader.meta.get("dtype")
    new_dataset = rio.open(
        dst_filename, "w", 
        driver = "GTiff",
        height = source_dataset_reader.shape[0],
        width = source_dataset_reader.shape[1],
        count = 1,
        nodata = nodata_value,
        dtype = dtype,
        crs = epsg_code,
        transform = source_dataset_reader.meta.get("transform")
    )
    new_dataset.write(numpy_array, 1)
    new_dataset.close()
    calc_stats(dst_filename)
    return dst_filename


def inverse_distance(proximity_raster_path, variable_value, output_dir, region_name, epsg_code, power_val):
    """Return filepath to output raster, created from NumPy array by calling array2raster function.

    Args:
        proximity_raster_path (str): Filepath to proximity raster.
        variable_value (int): The value representing the variable from the FWA, e.g. 1-7 Stream Order values.
        output_dir (str): Directory to write GeoTiff file into.
        region_name (str): Geographic region.
        epsg_code (int): EPSG code.
        power_val (float): Value to apply to raster np.power function (e.g., variable_value * np.power(1/prox_array, .25))

    Returns:
        str: Return filepath to output raster, created from NumPy array.
    """
    logger.info(MSG_BREAK)
    logger.info(f"Creating inverse distance raster for {region_name.upper()}...")
    # Create connection to raster and check that it is single-band; if it is, read raster band into in-memory numpy array.
    prox_connection = rio.open(proximity_raster_path)
    assert prox_connection.count == 1, f"{proximity_raster_path} is not a single-band raster."
    prox_array = prox_connection.read(1)
    # Replace zero values in proximity array with 1.0 to avoid dividing by zero resulting in "inf" values.
    prox_array[prox_array == 0.0] = 1.0
    # Apply raster algebra: Variable value multiplied by inverse distance n, raised to the power of power_val.
    inverse_distance_array = sqrt(variable_value) * np.power(1/prox_array, power_val)
    return array2raster(output_dir, inverse_distance_array, proximity_raster_path, f"{region_name}_weighted_inverse", epsg_code, -9999)


def mask_array(raster_to_mask, mask_raster_connection, output_dir, region_name, epsg_code, nodata_value):
    """Mask RASTER_TO_MASK by NoData values in MASK_RASTER_CONNECTION and write new array to disk as GeoTiff with NoData values set as NODATA_VALUE, 
       which might be different than the NoData value in RASTER_TO_MASK.

    Args:
        raster_to_mask (str): Path to raster to use be masked.
        mask_raster_connection (dataset reader): Connection to GeoTiff to be used as the mask (by NoData value).
        output_dir (str): Directory to write GeoTiff file into.
        region_name (str): Geographic region.
        epsg_code (int): EPSG code for GeoTIFF.
        nodata_value (int): Value to assign as NoData in output GeoTiff.

    Returns:
        str: Path to output masked raster.
    """
    logger.info("Applying mask...")
    basename = os.path.basename(raster_to_mask).split(".")[0]
    raster_mask_nodata = mask_raster_connection.meta.get("nodata")
    mask_array = mask_raster_connection.read(1)
    to_mask_connection = rio.open(raster_to_mask)
    to_mask_array = to_mask_connection.read(1)
    masked_array = np.where(mask_array == raster_mask_nodata, nodata_value, to_mask_array)
    masked_raster = array2raster(output_dir, masked_array, raster_to_mask, f"{basename}_masked", epsg_code, nodata_value)
    return masked_raster


def sum_rasters(output_dir, raster_file_list, region_name):
    """Given a list of file paths to GeoTIFFs, calculate a NumPy array of all rasters summed together and return file path to output raster.

    Args:
        output_dir (str): Directory to write GeoTiff file into.
        raster_file_list (list): List of file paths to GeoTIFFs.
        region_name (str): Geographic region.

    Returns:
        str: Filepath to output raster, created from NumPy array.
    """
    logger.info("Summing rasters...")
    src = rio.open(raster_file_list[0])
    dst_filename = os.path.join(output_dir, f"{region_name}_weighted_inverse_sum.tif")
    meta = src.meta
    with rio.open(dst_filename, "w", **meta) as dst:
        src_array = src.read(1)
        for raster in raster_file_list[1:]:
            with rio.open(raster) as next_src:
                next_array = next_src.read(1)
                src_array += next_array
        dst.write(src_array, 1)
    calc_stats(dst_filename)
    return dst_filename


def spatial_join(gdf_polys, gdf_points, variable):
    """Return GeoDataFrame that is a result of left joining point attributes to polygon GeoDataFrame for points within polygon. Drop duplicates.

    Args:
        gdf_polys (GeoDataFrame): Polygon GDF.
        gdf_points (GeoDataFrame): Points GDF.
        variable (str): FWA stream variable name: STREAM_ORDER or STREAM_MAGNITUDE.

    Returns:
        GeoDataFrame: GeoDataFrame that is a result of left joining point attributes to polygon GeoDataFrame for points within polygon. Drop duplicates.
    """
    gdf_polys = drop_all_but_geom(gdf_polys)
    gdf_polys_join = gdf_polys.sjoin(gdf_points, how="left")
    gdf_polys_join = gdf_polys_join[gdf_polys_join[variable].notna()]
    gdf_polys_join = gdf_polys_join.drop(columns=["index_right"])
    return gdf_polys_join


def rectangular_mesh(polygon_geom, resolution):
    """Construct a rectangular mesh of points within bounds of POLYGON_GEOM at a specified RESOLUTION.

    Args:
        polygon_geom (Polygon): Shapely polygon geometry.
        resolution (int): Resolution to space the gridded points at.

    Returns:
        list: List of Shapely point geometry objects (gridded mesh).
    """
    latmin, lonmin, latmax, lonmax = polygon_geom.bounds
    points = []
    for lat in np.arange(latmin, latmax, resolution):
        for lon in np.arange(lonmin, lonmax, resolution):
            points.append(Point(lat, lon))
    return points


def validate_points(polygon_geom, point_geom_list):
    """Return a subset of points from POINT_GEOM_LIST that are contained by POLYGON_GEOM.

    Args:
        polygon_geom (Polygon): Shapely polygon geometry.
        point_geom_list (list): List of Shapely point geometry objects (gridded mesh).
    Returns:
        list: List of Shapely point geometry objects that are contained within POLYGON_GEOM, not just its bounding box.
    """
    # Create prepared geometric object for more efficient batches of operations.
    prep_polygon = prep(polygon_geom)
    valid_points = []
    valid_points.extend(filter(prep_polygon.contains, point_geom_list))
    return valid_points


def add_pseudopoints(gdf_interp, gdf_points, variable, resolution, epsg_code, fn):
    """Return GDF_POINTS GeoDataFrame after additional pseudopoints have been appended to it. These additional points are created so that they fall 
       within or along the geometry of each feature of GDF_INTERP, depending on the geometry type.

    Args:
        gdf_interp (GeoDataFrame): GDF with either polygon or line geometry.
        gdf_points (GeoDataFrame): Points GDF.
        variable (str): FWA stream variable name: STREAM_ORDER or STREAM_MAGNITUDE.
        resolution (int): Resolution to space the gridded points at.
        epsg_code (int): EPSG code for GeoDataFrame.
        fn (function): function to apply to points.

    Returns:
        GeoDataFrame: GDF_POINTS GeoDataFrame after additional pseudopoints have been appended to it.
    """
    for index, row in gdf_interp.iterrows():
        row_geometry = row["geometry"]
        stream_var = int(row[variable])
        # Create rectangular mesh of points within bounding box of polygon or interpolate points along a line.
        points = fn(row_geometry, resolution)
        if isinstance(row_geometry, Polygon) or isinstance(row_geometry, MultiPolygon):
            # Validate if each point falls inside shape using a prepared polygon.
            points = validate_points(row_geometry, points)
        # Create GeoDataFrame of valid points and append to existing points GeoDataFrame.
        gdf_pseudopoints = gpd.GeoDataFrame({variable: stream_var, 
                                             "PSUEDOPOINT": 1,
                                             "geometry": points}, 
                                              crs=f"EPSG:{epsg_code}")
        gdf_points = gdf_points.append(gdf_pseudopoints)
    return gdf_points


def interpolate_points(line_string, resolution):
    """Construct point geometries along LINE_STRING geometry at a specified distance (RESOLUTION//2).

    Args:
        polygon_geom (Polygon): Shapely polygon geometry.
        resolution (int): Resolution to space the gridded points at.

    Returns:
        list: List of Shapely point geometry objects (gridded mesh).
    """
    distance_delta = resolution//2
    distances = np.arange(0, line_string.length, distance_delta)[:-1]
    return [line_string.interpolate(distance) for distance in distances] + [line_string.boundary[1]]


def drop_all_but_geom(input_gdf):
    """Return a new GeoDataFrame that is the INPUT_GDF stripped of all columns besides geometry.

    Args:
        input_gdf (GeoDataFrame): GDF to strip columns from.

    Returns:
        GeoDataFrame: A new GeoDataFrame that is the INPUT_GDF stripped of all columns besides geometry.
    """
    gdf_columns = list(input_gdf.columns)
    gdf_columns.remove("geometry")
    input_gdf = input_gdf.drop(columns=gdf_columns)
    return input_gdf


def max_within(gdf_polys, gdf_lines, variable):
    """For each river (multi)polygon, find the max VARIABLE value from rows of GDF_LINES whose geometry intersects the geometry 
    from GDF_POLYS and add it to a new column in GDF_POLYS.

    Args:
        gdf_polys (GeoDataFrame): GDF with polygon geometry.
        gdf_lines (GeoDataFrame): GDF with line geometry.
        variable (str): FWA stream variable name: STREAM_ORDER or STREAM_MAGNITUDE.
    """
    logger.info(MSG_BREAK)
    logger.info(f"Getting max variable value from streams contained in each river (multi)polygon.")

    max_values = []
    for index, row in gdf_polys.iterrows():
        row_geometry = row["geometry"]
        lines_within = gdf_lines.loc[gdf_lines.intersects(row_geometry)]
        if lines_within.shape[0] == 0:
            gdf_polys.drop(index, inplace=True)
            continue
        max_values.append(max(lines_within[variable]))
    gdf_polys[variable] = max_values
    return gdf_polys


def filter_streams(streams_gdf, rivers_gdf, within_rivers=False):
    """Filter STREAMS_GDF by clipping to RIVERS_GDF geometry or selecting streams whose geometry is not within a river"s geometry. 
    Convert geometry to 2D if 3D.
    https://gis.stackexchange.com/questions/279670/looking-for-geopandas-qgis-equivalent-of-arcgis-select-by-location-tool

    Args:
        streams_gdf (GeoDataFrame): GDF streams with polyline geometry.
        rivers_gdf (GeoDataFrame): GDF rivers with polygon geometry.
        within_rivers (boolean): True/False to filter streams within river polygons or not.

    Returns:
        GeoDataFrame: Stream network GeoDataframe after filtering is applied.
    """
    if within_rivers:
        streams_gdf = gpd.clip(streams_gdf, rivers_gdf, keep_geom_type=True)
        streams_gdf["within_river_poly"] = 1
    else:
        # Two part geometry filter. First: get streams that do not cross river polygon. Second: subset those that are not within river polygon.
        streams_gdf = streams_gdf[~streams_gdf.geometry.crosses(rivers_gdf.geometry.unary_union)]
        streams_gdf = streams_gdf[~streams_gdf.geometry.within(rivers_gdf.geometry.unary_union)]
        streams_gdf["within_river_poly"] = 0
    # streams_gdf = streams_gdf.drop(columns=["index_right"])
    if is_3d(streams_gdf):
        streams_gdf = gdf_2d(streams_gdf)
    return streams_gdf


def gdal_invdist(interp_points, raster_path, region_name, power_val, radius_val, stream_var, output_dir, epsg_code):
    """Interpolate points using Inverse Distance Weighted (GDAL invdist) function.
    https://gis.stackexchange.com/questions/313191/how-to-use-gdalgridinversedistancetoapower-in-a-python-script
    # https://gis.stackexchange.com/questions/396995/using-geopandas-geodataframe-in-gdal-grid-for-spatial-interpolation-viz-idw-nea

    Args:
        interp_points (str): Points used for interpolation.
        raster_path (dataset reader): Full filepath to input GeoTiff.
        region_name (str): Geographic region.
        power_val (float): Power value to apply to interpolated surface for IDW. 
        radius_val (float): Radius for interpolation circle.
        stream_var (str): FWA stream variable name: STREAM_ORDER or STREAM_MAGNITUDE.
        output_dir (str): Directory to write GeoTiff file into.
        epsg_code (int): EPSG code.

    Returns:
        str: Path to output interpolated raster.
    """
    logger.info("----------INTERPOLATION----------")
    algo = f"invdist:power={power_val}:radius1:={radius_val}:radius2:{radius_val}"
    logger.info(algo)
    raster_ds = gdal.Open(raster_path)
    ulx, xres, xskew, uly, yskew, yres  = raster_ds.GetGeoTransform()
    lrx = ulx + (raster_ds.RasterXSize * xres)
    lry = uly + (raster_ds.RasterYSize * yres)
    out_bounds = [ulx, uly, lrx, lry]
    logger.info(out_bounds)
    x_size, y_size = raster_ds.RasterXSize, raster_ds.RasterYSize
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg_code)
    power_str = str(power_val).replace(".", "")
    radius_str = str(radius_val)
    output_filepath = os.path.join(output_dir, f"{region_name}_idw_{power_str}_{radius_str}.tif")
    interpolated_raster = gdal.Grid(output_filepath, gdal.OpenEx(interp_points.to_json(), gdal.OF_VECTOR), 
                                    format="GTiff", outputBounds=out_bounds, outputType=gdal.GDT_Float32,
                                    outputSRS=srs,
                                    algorithm=algo, 
                                    width=x_size, height=y_size, zfield=stream_var)
    interpolated_raster = None
    raster_ds = None
    return output_filepath


def convolve_raster(raster_path, wmin, wmax, output_dir, region_name, epsg_code, nodata_value):
    """Create a numpy meshgrid with shape WINDOW_SIZE of weight values used to convolve over array from RASTER_PATH.
    Write convolved array to disk as new GeoTIFF file.

    ATTRIBUTION: https://gitlab.com/dfo-msea/environmental-layers/bathy-uncertainty/-/blob/master/testing-focalstats.ipynb
    size = 5
    hor = abs( abs(np.mgrid[-1:1:size/1j]) - 1 ) 
    xw,yw = np.meshgrid(hor, hor.T)
    weights = xw * yw
    weights
    array([[0.  , 0.  , 0.  , 0.  , 0.  ],
       [0.  , 0.25, 0.5 , 0.25, 0.  ],
       [0.  , 0.5 , 1.  , 0.5 , 0.  ],
       [0.  , 0.25, 0.5 , 0.25, 0.  ],
       [0.  , 0.  , 0.  , 0.  , 0.  ]])

    Args:
        raster_path (str): Path to raster file.
        wmin (int): Integer used to define minimum moving window for convolution.
        wmax (int): Integer used to define maximum moving window for convolution.
        output_dir (str): Directory to write GeoTiff file into.
        region_name (str): Geographic region string.
        epsg_code (int): EPSG projection code.
        nodata_value: Used as NoData value in output raster.
    """
    def create_window_sizes(wmin, wmax, n_bins):
        """Return dict of odd-valued integers,  spaced evenly on a log scale (a geometric progression using numpy.geomspace) 
            using WMIN and WMAX as minimum and maximum using N_BINS as number of bins.
        >>> create_window_sizes(51, 301, 10)
        {1: 51, 2: 63, 3: 77, 4: 93, 5: 113, 6: 137, 7: 167, 8: 203, 9: 247, 10: 301}
        """
        bin_list = []
        for i in range(1, 11):
            bin_list.append(i)
        bin_vals = list(np.round(np.geomspace(wmin, wmax, n_bins)).astype(int))
        make_odd = lambda x: x+1 if x%2 == 0 else x
        bin_vals = [make_odd(val) for val in bin_vals]
        return dict(zip(bin_list, bin_vals))
    
    def create_weights_array(window_size):
        horizontal_array = abs( abs(np.mgrid[-1:1:window_size/1j]) - 1 )    
        xw, yw = np.meshgrid(horizontal_array, horizontal_array)
        return xw * yw

    def add_convolved(src_array, raster_list):
        # Create array of zeros the same shape as source_array.
        convolve_array = np.zeros(src_array.shape)
        for r in raster_list:
            r_name = os.path.basename(r)
            logger.info(f"Adding array {r_name} to final convolved array.")
            # Convert to np array.
            source_ds = rio.open(r)
            source_array = source_ds.read(1)
            # Add to convolve_array.
            convolve_array = np.add(convolve_array, source_array)
        return convolve_array

    
    source_ds = rio.open(raster_path)
    source_array = source_ds.read(1)
    unique_vals = list(np.unique(source_array))[1:]
    unique_vals.sort(reverse=True)
    # Create window sizes for each unique value.
    windows_dict = create_window_sizes(wmin, wmax, 10)
    # List for convolved rasters.
    r_list = []
    for val in unique_vals:
        # Free up some memory.
        logger.info("RAM memory used: {}%".format(psutil.virtual_memory()[2]))
        window = windows_dict.get(val)
        logger.info(f"Performing convolution for value: {val} and window shape: {window}")
        tmp_array = np.where(source_array == val, source_array, 0)
        # Transform values in array by raising to the power of ___. The idea here is the reduce the range of values.
        tmp_array = np.power(tmp_array, .05)
        weights_array = create_weights_array(window)
        tmp_convolved = ndi.convolve(tmp_array, weights_array, mode="reflect")
        tmp_raster = array2raster(output_dir, tmp_convolved, raster_path, f"{region_name}_convolve_{val}", epsg_code, nodata_value, "float32")
        r_list.append(tmp_raster)
    convolved = add_convolved(source_array, r_list)
    return array2raster(output_dir, convolved, raster_path, f"{region_name}_convolve_{wmin}_{wmax}", epsg_code, nodata_value, "float32")
    

def process(geo_region, fwa_container, filter_file_name, out_gpkg_name, method, power_value, radius_value, wmin, wmax):
    logger.info(">>>>>>>>>>PARAMETERS<<<<<<<<<<", extra={"simple": True})
    param_table = BeautifulTable()
    param_table.columns.header = ["geo_region", "fwa_container", "filter_file_name", "out_gpkg_name", "method"]
    param_table.rows.append([geo_region, fwa_container, filter_file_name, out_gpkg_name, method])
    logger.info(param_table, extra={"simple": True})
    logger.info("\n")
    pow_str = str(power_value).replace(".", "") if method in (["interpolate", "inverse_proximity"]) else ""
    radius_str = str(radius_value).replace(".", "") if radius_value is not None else ""
    window_str = str(wmin) + "_" + str(wmax) if wmin is not None else ""
    output_dir = make_dir(data_dir, "output_" + geo_region + "_" + method + "_" + pow_str + window_str + "_" + radius_str + "_" + DATE)
    filter_spatial_type = spatial_type(filter_file_name)
    filter_layer = load_raster(data_dir, filter_file_name)
    filter_bbox = get_bbox(filter_layer)
    cell_res = cell_resolution(data_dir, filter_file_name)
    stream_var = "STREAM_ORDER"
    epsg_value = get_epsg(filter_bbox)
    coast = load_vector(data_dir, fwa_container, layer_name=fwa_lyr_names.get("coast"))
    if is_3d(coast):
        coast = gdf_2d(coast)
    simplified_boundary = load_vector(data_dir, fwa_container, layer_name=geo_region)
    streams_clipped = gpd.clip(load_vector(data_dir, fwa_container, layer_name=fwa_lyr_names.get("streams"), bbox=filter_bbox), simplified_boundary)
    rivers = gpd.clip(load_vector(data_dir, fwa_container, layer_name=fwa_lyr_names.get("rivers"), bbox=filter_bbox), simplified_boundary, keep_geom_type=True)
    streams_within, streams_not_within = filter_streams(streams_clipped, rivers, within_rivers=True), filter_streams(streams_clipped, rivers, within_rivers=False)
    rivers = max_within(rivers, streams_within, stream_var)
    gdf_intersection_points = intersect_lines(coast, streams_not_within) 
    gdf_intersection_points = add_pseudopoints(streams_not_within, gdf_intersection_points, stream_var, cell_res//2, epsg_value, interpolate_points)
    gdf_intersection_points = add_pseudopoints(rivers, gdf_intersection_points, stream_var, cell_res//2, epsg_value, rectangular_mesh)
    out_points, out_lines = "fwa_stream_points_" + geo_region, "fwa_streams_subset_" + geo_region
    gpkg_path = write_gpkg(gdf_intersection_points, output_dir, out_gpkg_name, out_points)
    all_streams = gpd.GeoDataFrame(pd.concat([streams_within, streams_not_within], ignore_index=True), crs=streams_within.crs)
    write_gpkg(all_streams, output_dir, out_gpkg_name, out_lines)
    raster_path = os.path.join(data_dir, filter_file_name)
    if method == "interpolate":
        idw = gdal_invdist(gdf_intersection_points, raster_path, geo_region, power_value, radius_value, stream_var, output_dir, epsg_value)
        masked_raster = mask_array(idw, filter_layer, output_dir, geo_region, epsg_value, -9999)
    elif method == "inverse_proximity":
        logger.info(">>>>>>>>>>INVERSE PROXIMITY<<<<<<<<<<", extra={"simple": True})
        logger.info(f"sqrt(variable_value) * np.power(1/prox_array, {power_value})", extra={"simple": True})
        stream_variable_raster = vector_to_raster(raster_path, gpkg_path, out_points, output_dir, 0, geo_region, stream_var, epsg_value)
        stream_unique_var_values = stream_ord_mag(gdf_intersection_points, stream_var)
        stream_proximity_rasters = [proximity_raster(stream_variable_raster, value, output_dir, geo_region + "_" + str(value)) for value in stream_unique_var_values]
        inverse_distance_rasters = [inverse_distance(prox_raster, value, output_dir, geo_region + f"_{value}", epsg_value, power_value) for value, prox_raster in zip(stream_unique_var_values, stream_proximity_rasters)]
        sum_raster = sum_rasters(output_dir, inverse_distance_rasters, geo_region)
        masked_raster = mask_array(sum_raster, filter_layer, output_dir, geo_region, epsg_value, -9999)
    else:
        logger.info(">>>>>>>>>>CONVOLVE<<<<<<<<<<", extra={"simple": True})
        logger.info(f"Using {wmin} and {wmax} to create minimum and maximum numpy array windows to hold weight values for convolution.)", extra={"simple": True})
        stream_variable_raster = vector_to_raster(raster_path, gpkg_path, out_points, output_dir, 0, geo_region, stream_var, epsg_value)
        # Free up some memory.
        logger.info("RAM memory used: {}%".format(psutil.virtual_memory()[2]))
        logger.info("Freeing some memory...")
        del filter_bbox, coast, simplified_boundary, streams_clipped, rivers, streams_within, gdf_intersection_points, out_points, out_lines, all_streams
        logger.info("RAM memory used: {}%".format(psutil.virtual_memory()[2]))
        convolved_raster = convolve_raster(stream_variable_raster, wmin, wmax, output_dir, geo_region, epsg_value, -9999)
        masked_convolve = mask_array(convolved_raster, filter_layer, output_dir, geo_region, epsg_value, -9999)
