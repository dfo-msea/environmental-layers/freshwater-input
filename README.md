# Freshwater influence weighted by stream order

__Main author:__  Cole Fields   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
  + [Data preparation](#data-preparation)
  + [Create bounding box for spatial query](#create-bounding-box-for-spatial-query)
  + [Load vector layers into memory as GeoDataFrames](#load-vector-layers-into-memory-as-geodataframes)
  + [Overlay layers to get river mouths](#overlay-layers-to-get-river-mouths)
  + [(Optionally) Create pseudopoints](#create-pseudopoints)
  + [Convert intersection points to raster](#convert-points-to-raster)
  + [Apply Interpolation Method](#interpolate-method)
  + [Apply Inverse Distance Method](#inverse-proximity-method)
  + [Apply Convolve Method](#convolve-method)

- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Notes](#notes)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Develop regional raster layers representing relative freshwater influence from freshwater atlas data weighted by stream order. Layers are developed for use as environmental predictor layers for eelgrass species distribution model.


## Summary
The primary data inputs for the simple freshwater input layer are the stream networks, river polygons, and coastline from the Freshwater Atlas - Province of British Columbia (gov.bc.ca) database (FWA). The output is a nearshore raster (up to 50 m depth, within 5 km from shore) in the marine environment. The cell values represent a general indication of freshwater influence where a higher cell value represents greater freshwater influence. Different methods can be applied (positional argument `method`) to derive the results. The convolve method will create window sizes that vary depending on the stream order value, but are consistent between 
regions (i.e., for a stream order 6, the window size will be the same between regions). In order to have results that are comparable between regions (i.e., a cell value represents the same thing between 
different regions), layers can be normalized using the script here: https://gitlab.com/dfo-msea/environmental-layers/normalize-fetch. 


## Status
In-development


## Contents
There are two python files, and a yml file. The `environment.yml` file contains the information for creating an environment to execute the python scripts. `freshwater_utils.py` contains the functions for all of the processing. `freshwater_input.py` is what is run from the command line with arguments. Run `python freshwater_input.py -h` for help.


## Methods
##### Data preparation
* Data preparation included downloading the FWA datasets. The feature classes in the FWA_STREAM_NETWORKS_SP.gdb were merged into a single layer and saved as a GeoPackage layer. FWA river polygons and the FWA coastline were also exported as layers in the same GeoPackage.
* River polygons were further prepared by running the Dissolve Boundaries tool in ArcPro. The logic behind this decision was that a river polygon should be assigned the maximum 
stream order value from the FWA lines within it. Without dissolving, when a river splits near the mouth, one or more splits will have a very low order while one split will retain a higher order. By dissolving the shared boundaries and assigning the max order value to that polygon, we address this problem.
* The Fraser river dissolved polygon was then buffered by 6 km and manually edited to extend into the delta. Ideally, a larger window size would be used for the convolution rather than buffering the river polygon, but computational limitations led to this modification as an alternative.
* Copy existing simplified boundary feature classes into GeoPackage.
* Use existing bathymetry rasters from SDM work (nearshore, restricted to 50 m depth and 5 km from shore).

##### Create bounding box for spatial query
* Load a raster layer into memory as a dataset reader object (rasterio).
* Create a bounding box from spatial data to use as a spatial filter against the FWA stream network.

##### Load vector layers into memory as GeoDataFrames
* FWA coastline.
* FWA stream network, filtered by bounding box geometry.
* Load simplified boundary geometry from source GeoPackage. Argument `geo_region` is used to match a layer from within the GeoPackage. 
* FWA rivers, filtered by bounding box geometry, and clipped to simplified boundary layer (footprint of regional SDM predictors).
* Flatten geometry of vector layers to 2D (if geometry is 3D).

##### Overlay layers to get river mouths
* Intersect filtered and FWA streams (polylines) and FWA coastline (polylines).
* Do not keep the geometry type when running the overlay, since the geometry type we want is points at the intersections of streams and the coastline.
* Remove duplicate rows based on geometry column from GeoDataFrame with intersection results.

##### Create pseudopoints
* Spatial join the river mouths (points) to river polygons in order to attach stream variable (e.g., STREAM_ORDER).
* After join, clip river polygons by simplified boundary geometry. This is to reduce processing time for creating the pseudopoints.
* Iterate over rows (river polygons) and create a rectangular mesh of points within the bounds of the clipped river geometry. Remove points that are not contained by the actual boundary of the clipped river polygon. Add these points as GeoDataFrame with the stream variable (e.g., STREAM_ORDER and its value) to existing river mouth points.
* Add pseudopoints to streams (FWA stream network polylines) along each line (after lines are clipped to simplified boundary geometry). Points are generated (interpolated) along the length of each line at 1/2 of the cell size of the `filter_file` argument. 

##### Convert points to raster
* Write the intersected points GeoDataFrame to disk as layer in GeoPackage (this is done after creating psuedopoints for the rivers if that option is selected).
* Use the attribute STREAM_ORDER to create a raster with integer values for the stream order variable (NoData is represented by 0 for the output raster).

### Interpolate Method
* Interpolate points using Inverse Distance Weighted (GDAL invdist) function.
* Function takes a radius (distance to search for points within) and a power value to apply to IDW function.
* See https://gdal.org/programs/gdal_grid.html#gdal-grid-invdist for details.

### Inverse Proximity Method
##### Create inverse distance rasters
* Create list of unique values from <stream_var> in GeoDataFrame (e.g., unique STREAM_ORDER values.)
* For each unique value, create a proximity raster. A cell value in the output raster represents the distance from that cell to a cell that matches the unique value. This will generate one proximity raster for each unique value.
* Next, create inverse distance rasters. For each of the proximity rasters, replace any values in the array that are 0.0 with 1.0 to avoid division by zero. Apply raster algebra: square root of variable value multiplied by inverse distance n, raised to the power of 1/2 (equivalent to 1/sqrt(n)).

##### Sum inverse distance rasters and mask
* Then, sum the inverse distance arrays together, write to disk as GeoTIFF.
* Mask the data by filter raster and write masked array to disk as GeoTIFF.

### Convolve Method
* Open stream variable raster, read band 1 as numpy array.
* Apply np.power(array, .05) function to array to decrease the range of values.
* Create windows (np.meshgrid) of weights based on wmin and wmax values and the number of desired bins. To ensure that the same stream order For each stream order, a different sized window is used. For example, stream order 7 uses a larger window than stream order 1. To ensure that the window size for a given stream order is the same between regions, 10 bins are created (Fraser river has stream order 10 which is the max of any regions) and scaled using numpy's geomspace function that return numbers spaced evenly on a log scale (a geometric progression).
* For each stream order, subset array by that particular value, then use ndi.convolve (https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.convolve.html) over existing array with a unique window size.
* On each iteration, sum the existing convolved array with the current convolved array.
* Write convolved array to disk as GeoTiff.


## Requirements
__Software__
* Requires the following packages: osgeo (gdal, ogr), numpy, geopandas, shapely, and rasterio, math. 
* See environment.yml file.

__Source Data__
 * Freshwater stream network, coast lines, and river polygons from the FWA FTP site: ftp://ftp.geobc.gov.bc.ca/sections/outgoing/bmgs/FWA_Public
 * Boundaries, simplified from: https://www.gis-hub.ca/dataset/sdm-boundaries


## Caveats
* Output rasters for each region are further processed with: https://gitlab.com/dfo-msea/environmental-layers/normalize-fetch to normalize on a 0-1 scale where only the region with the greatest value is 0-1 and the other regions are scaled relatively. This is done to ensure that values in one region can be compared to values in another region. 
* May have limited utility. The products derived from these scripts are in development and may only be suitable for a small range of suitable applications. 


## Uncertainty
* The values in the rasters are not a measure a salinity. The units are an index representing the level of freshwater influence (if using the convolve method), or proximity to a freshwater source, weighted by the source order (for the other methods), and rescaled across regions. 
* Stream Order may be more appropriate for the convolve method and Stream Magnitude may be more appropriate for the other methods.
* Does not account for depth in any way. 


## Notes
* Stream magnitude refers to the number of tributaries flowing into a given stream segment. For example, if three tributaries flow into a stream then it is assigned a magnitude of 3 in the FWA (Gray, 2010).
* Stream order refers to a a rank of a stream based on size and complexity. Stream ordering is the process of identifying and grouping stream segments. The freshwater atlas (FWA) uses the Strahler (1952) approach to stream ordering (Gray, 2010). The smallest channels are first-order (order 1). In general, when two channels of order n combine, the new channel is order n + 1. When a lower order channel joins a higher order channel, the resulting channel is the higher order value. Stream magnitude refers to the number of tributaries that flow into a given stream segment. 


## Acknowledgements
* Ashley Park
* Jessica Nephin
* Daniel Weller


## References
* https://gitlab.com/dfo-msea/environmental-layers/bathy-uncertainty/-/blob/master/testing-focalstats.ipynb
* Gray M. 2010. Freshwater Water Atlas User Guide. Integrated Land Management Bureau. https://www2.gov.bc.ca/assets/gov/data/geographic/topography/fwa/fwa_user_guide.pdf
* Rasterize vector layer: https://opensourceoptions.com/blog/use-python-to-convert-polygons-to-raster-with-gdal-rasterizelayer/
* GeoPandas line intersections: https://gis.stackexchange.com/questions/413976/geopandas-road-intersections-crossroads?noredirect=1&lq=1
* NumPy power: https://www.sharpsightlabs.com/blog/numpy-power/
* Raster to array: https://geobgu.xyz/py/rasterio.html#creating-raster-from-array
* NumPy where: https://numpy.org/doc/stable/reference/generated/numpy.where.html
* Create gridded points within polygon: https://stackoverflow.com/questions/66010964
fastest-way-to-produce-a-grid-of-points-that-fall-within-a-polygon-or-shape
* Shapely prepared geometric objects: https://shapely.readthedocs.io/en/stable/manual.html
* Inverse distance weighted interpolation: https://pro.arcgis.com/en/pro-app/latest/help/analysis/geostatistical-analyst/how-inverse-distance-weighted-interpolation-works.htm
* Points evenly spaced along a line: https://stackoverflow.com/questions/62990029/how-to-get-equally-spaced-points-on-a-line-in-shapely
* Convolve math: https://stackoverflow.com/questions/37969197/math-behind-scipy-ndimage-convolve
* Evenly spaced bins on log scale: https://numpy.org/doc/stable/reference/generated/numpy.geomspace.html
